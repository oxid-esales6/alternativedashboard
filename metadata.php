<?php

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id' => 'alternative_dashboard',
    'title' => '<b>a</b>lternative <b>d</b>ash<b>b</b>oard',
    'description' => array(
        'de' => 'Stellt ein alternatives Dashboard zur Verfügung.<br><br>Weitere Software unter <a href="https://www.petit-souris.de" target="_blank">https://www.petit-souris.de</a> oder <a href="https://gitlab.petit-souris.de" target="_blank">https://gitlab.petit-souris.de</a>, Fragen, Wünsche, Jobangebote gern unter <a href="mailto:oxid@petit-souris.dee">oxid@petit-souris.de</a>.<br><a href="https://paypal.me/hpeterseim/1" target=""blank">Und wenn Sie möchten können Sie natürlich auch gerne einen Euro spenden.</a>',
        'en' => 'Delivers an alternative dashboard.<br><br>More peaces of software under <a href="https://www.petit-souris.de" target="_blank">https://www.petit-souris.de</a> or <a href="https://gitlab.petit-souris.de" target="_blank">https://gitlab.petit-souris.de</a>, questions, wishes or job offers goes to <a href="mailto:oxid@petit-souris.de">oxid@petit-souris.de</a>.<br><a href="https://paypal.me/hpeterseim/1" target=""blank">Spend an euro if you want.</a>'
    ),
    'thumbnail' => 'views/admin/image.php',
    'version' => '1.0.1',
    'author' => 'Hannes Peterseim',
    'email' => 'oxid@petit-souris.de',
    'url' => 'https://www.petit-souris.de',

    'events' => array(
    ),

    'files' => array(
    ),

    'extend' => array(
        \OxidEsales\Eshop\Application\Controller\Admin\NavigationController::class => hpeterseim\alternativedashboard\src\controller\admin\alternativedashboard::class
    ),

    'templates' => [
        'dashboard.tpl' => 'hpeterseim/alternativedashboard/views/admin/tpl/dashboard.tpl',
    ],

    'blocks' => array(
    ),

    'settings' => array(
        array('group' => 'adbOptions', 'name' => 'showDailySales', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showDailyArticlesAmount', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showDailyArticlesList', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showDailyDiscounts', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showDailyManufacturers', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showDailyPayments', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showDailyDeliveries', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showDailyUsers', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'showBaseData', 'type' => 'bool', 'value' => '1'),
        array('group' => 'adbOptions', 'name' => 'chooseColor', 'type' => 'select', 'constrains' => '#808080|#008000|#ff0000|#000000|#4169E1|#FF1493|#FF8C00|#FFD700|#800000|#008B8B', 'value' => '#808080'),
        array('group' => 'adbOptions', 'name' => 'dashboardRefresh', 'type' => 'select', 'constrains' => '10|20|30|40|50|60|0', 'value' => '60'),
        array('group' => 'adbOptions', 'name' => 'showSparklines', 'type' => 'select', 'constrains' => '7|14|21|30|0', 'value' => '14'),

    )
);

