#adb alias alternative dashboard

## Systemvoraussetzung
Eine installierte GD Erweiterung wird vorausgesetzt.

## Installation :
1. Geben Sie auf der Konsole den Befehl 'composer config repositories.hpeterseim/alternativedashboard git https://gitlab.petit-souris.de/oxid-esales6/alternativedashboard.git' ein um composer mit dem Modul bekannt zu machen.
2. geben Sie nun 'composer require hpeterseim/alternativedashboard:"dev-master"' und installieren Sie so das Modul.
3. Gehen Sie im Backend in den Bereich Erweiterungen (bei Oxid EE im jeweiligen Subshop) und aktivieren Sie das Modul. Konfigurieren Sie das Dashboard gegebenenfalls.
4. Klicken Sie auf den "Home" Link  Hauptnavigation um das neue Dashboard zu sehen.


##Lizenz
 
###sparkline:

  Ordner alternative_dashboard/src/service/sparkline mit sparkline.php Copyright (c) 2014 Jamie Bicknell - @jamiebicknell
  
  -----------------------------------
  Lizenz:
  The MIT License (MIT)
