<?php
/**
 * Copyright 2019 by Hannes Peterseim
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Questions, job offers or simply wishes for this product are welcome, please use oxid@petit-souris.de or take a look @ https://gitlab.petit-souris.de for other peaces of software.
 */

namespace hpeterseim\alternativedashboard\src\controller\admin;

use OxidEsales\Eshop\Core\Registry;
use \OxidEsales\Eshop\Core\DatabaseProvider;

/**
 * Class alternative_dashboard
 */
class alternativedashboard extends alternativedashboard_parent
{

    /**
     * @var bool
     */
    private $isMall = false;

    /**
     * @var string
     */
    private $shopIdWhere = '';

    /**
     * @var int
     */
    private $dailySales = 0;

    /**
     * @var string
     */
    private $dailySalesSparklines = '';

    /**
     * @var int
     */
    private $dailyNetSales = 0;

    /**
     * @var string
     */
    private $dailyNetSalesSparklines = '';

    /**
     * @var int
     */
    private $dailyBrutSales = 0;

    /**
     * @var string
     */
    private $dailyBrutSalesSparklines = '';

    /**
     * @var int
     */
    private $dailyDiscounts = 0;

    /**
     * @var int
     */
    private $dailyDiscountsSum = 0;

    /**
     * @var int
     */
    private $dailyDiscountsPercent = 0;

    /**
     * @var int
     */
    private $dailyVoucherDiscounts = 0;

    /**
     * @var int
     */
    private $dailyVoucherDiscountsSum = 0;

    /**
     * @var int
     */
    private $dailyVoucherDiscountsPercent = 0;

    /**
     * @var string
     */
    private $dailyDiscountsSparklines = '';

    /**
     * @var string
     */
    private $dailyDiscountsSumSparklines = '';

    /**
     * @var string
     */
    private $dailyVoucherDiscountsSparklines = '';

    /**
     * @var string
     */
    private $dailyVoucherDiscountsSumSparklines = '';

    /**
     * @var int
     */
    private $dailyArticlesAmount = 0;

    /**
     * @var array
     */
    private $dailyArticlesList = array();

    /**
     * @var null
     */
    private $dailySalesCurrency = null;

    /**
     * @var array
     */
    private $dailyDeliveries = array();

    /**
     * @var string
     */
    private $dailyDeliveriesAmountSparklines = '';

    /**
     * @var string
     */
    private $dailyDeliveriesSumSparklines = '';

    /**
     * @var array
     */
    private $dailyPayments = array();

    /**
     * @var string
     */
    private $dailyPaymentsAmountSparklines = '';

    /**
     * @var string
     */
    private $dailyPaymentsSumSparklines = '';

    /**
     * @var int
     */
    private $dailyUsersAmount = 0;

    /**
     * @var int
     */
    private $dailyUsersNlSubscribed = 0;

    /**
     * @var int
     */
    private $dailyUsersNlUnsubscribed = 0;

    /**
     * @var string
     */
    private $dailyUsersAmountSparklines = '';

    /**
     * @var string
     */
    private $dailyUsersNlSubscribedSparklines = '';

    /**
     * @var string
     */
    private $dailyUsersNlUnsubscribedSparklines = '';

    /**
     * @var int
     */
    private $baseDataUser = 0;

    /**
     * @var int
     */
    private $baseDataSales = 0;

    /**
     * @var int
     */
    private $baseDataArticles = 0;

    /**
     * @var int
     */
    private $baseDataCategories = 0;

    /**
     * @var int
     */
    private $baseDataManufacturers = 0;

    /**
     * @var array
     */
    private $sparklineDays = array();

    /**
     * @var null
     */
    private $myConfig = null;

    /**
     * @var null
     */
    private $myShopId = null;

    /**
     * @var null
     */
    private $myDb = null;

    /**
     * Allowed host url
     *
     * @var string
     */
    protected $_sAllowedHost = "http://admin.oxid-esales.com";

    /**
     * Executes parent method parent::render(), generates menu HTML code,
     * passes data to Smarty engine, returns name of template file "nav_frame.tpl".
     *
     * @return string
     */
    public function render()
    {
        parent::render();
        $myUtilsServer = Registry::getUtilsServer();

        $sItem = Registry::getConfig()->getRequestParameter("item");
        $sItem = $sItem ? basename($sItem) : false;
        if (!$sItem) {
            $sItem = "nav_frame.tpl";
            $aFavorites = Registry::getConfig()->getRequestParameter("favorites");
            if (is_array($aFavorites)) {
                $myUtilsServer->setOxCookie('oxidadminfavorites', implode('|', $aFavorites));
            }
        } else {
            $oNavTree = $this->getNavigation();

            // set menu structure
            $this->_aViewData["menustructure"] = $oNavTree->getDomXml()->documentElement->childNodes;

            // version patch string
            $this->_aViewData["sVersion"] = $this->_sShopVersion;

            //checking requirements if this is not nav frame reload
            if (!Registry::getConfig()->getRequestParameter("navReload")) {
                // #661 execute stuff we run each time when we start admin once
                if ('home.tpl' == $sItem) {
                    if ('home.tpl' == $sItem) {
                        $this->_aViewData['aMessage'] = $this->_doStartUpChecks();
                        $sItem = 'dashboard.tpl';
                        if (!$this->isMall) {
                            $this->myShopId = Registry::getSession()->getVariable("actshop");
                            $this->shopIdWhere = "='" . $this->myShopId . "'";
                        } else {
                            $this->shopIdWhere = "!=''";
                        }
                        $this->myDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
                        $this->dailySalesCurrency = $this->myDb->getOne("SELECT OXCURRENCY FROM oxorder WHERE (OXSHOPID$this->shopIdWhere)");
                        $this->myConfig = Registry::getConfig();
                        $this->myShopId = Registry::getSession()->getVariable("actshop");
                        $this->isMall = $this->myConfig->isMall();
                        if ($this->myConfig->getConfigParam('showSparklines') > 0) {
                            $this->getSparklineDays();
                        }
                        if ($this->myConfig->getConfigParam('showDailySales')) {
                            $this->getDailySales();
                        }
                        if ($this->myConfig->getConfigParam('showDailyArticlesAmount')) {
                            $this->getDailyArticlesAmount();
                        }
                        if ($this->myConfig->getConfigParam('showDailyArticlesList')) {
                            $this->getDailyArticlesList();
                        }
                        if ($this->myConfig->getConfigParam('showDailyDiscounts')) {
                            $this->getDailyDiscounts();
                        }
                        if ($this->myConfig->getConfigParam('showDailyManufacturers')) {
                            $this->getDailyManufacturers();
                        }
                        if ($this->myConfig->getConfigParam('showDailyPayments')) {
                            $this->getDailyPayments();
                        }
                        if ($this->myConfig->getConfigParam('showDailyDeliveries')) {
                            $this->getDailyDeliveries();
                        }
                        if ($this->myConfig->getConfigParam('showDailyUsers')) {
                            $this->getDailyUsers();
                        }
                        if ($this->myConfig->getConfigParam('showBaseData')) {
                            $this->getBaseData();
                        }
                        $this->_aViewData['dashboardColor'] = $this->myConfig->getConfigParam('chooseColor');
                        $this->_aViewData['dashboardRefresh'] = $this->myConfig->getConfigParam('dashboardRefresh');
                    }
                }
            } else {
                //removing reload param to force requirements checking next time
                Registry::getSession()->deleteVariable("navReload");
            }
            // favorite navigation
            $aFavorites = explode('|', $myUtilsServer->getOxCookie('oxidadminfavorites'));

            if (is_array($aFavorites) && count($aFavorites)) {
                $this->_aViewData["menufavorites"] = $oNavTree->getListNodes($aFavorites);
                $this->_aViewData["aFavorites"] = $aFavorites;
            }

            // history navigation
            $aHistory = explode('|', $myUtilsServer->getOxCookie('oxidadminhistory'));
            if (is_array($aHistory) && count($aHistory)) {
                $this->_aViewData["menuhistory"] = $oNavTree->getListNodes($aHistory);
            }

            // open history node ?
            $this->_aViewData["blOpenHistory"] = Registry::getConfig()->getRequestParameter('openHistory');
        }

        $blisMallAdmin = Registry::getSession()->getVariable('malladmin');
        $oShoplist = oxNew(\OxidEsales\Eshop\Application\Model\ShopList::class);
        if (!$blisMallAdmin) {
            // we only allow to see our shop
            $iShopId = Registry::getSession()->getVariable("actshop");
            $oShop = oxNew(\OxidEsales\Eshop\Application\Model\Shop::class);
            $oShop->load($iShopId);
            $oShoplist->add($oShop);
        } else {
            $oShoplist->getIdTitleList();
        }

        $this->_aViewData['shoplist'] = $oShoplist;
        return $sItem;
    }

    /**
     * delivers days for sparklines
     */
    protected function getSparklineDays ()
    {
        $begin = time()-(($this->myConfig->getConfigParam('showSparklines')-1)*86400);
        for($i=0;$i<$this->myConfig->getConfigParam('showSparklines');$i++) {
            $this->sparklineDays[] = date('Y-m-d',$begin+($i*86400));
        }
    }

    /**
     * delivers todays sale data
     */
    protected function getDailySales ()
    {
        $this->dailySales = $this->myDb->getOne("SELECT count(*) as SALES FROM oxorder WHERE (OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailySales == '') {
            $this->dailySales = '0';
        }
        $this->_aViewData['dailySales'] = $this->dailySales;

        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach($this->sparklineDays as $day) {
                $temp = $this->myDb->getOne("SELECT count(*) as SALES FROM oxorder WHERE (OXORDERDATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($temp == '') {
                    $temp = '0';
                }
                if ($this->dailySalesSparklines == '') {
                    $this->dailySalesSparklines .= (int) $temp;
                } else {
                    $this->dailySalesSparklines .= ',' . (int) $temp;
                }
            }
            $this->_aViewData['dailySalesSparklines'] = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailySalesSparklines . '&line=' . str_replace('#','',$this->myConfig->getConfigParam('chooseColor'));
        }

        $this->dailyNetSales = $this->myDb->getOne("SELECT SUM(OXTOTALNETSUM) FROM oxorder WHERE (OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyNetSales == '') {
            $this->dailyNetSales = '0,00';
        }
        $this->_aViewData['dailyNetSales'] = number_format($this->dailyNetSales,2,',','') . ' ' . $this->dailySalesCurrency;

        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach($this->sparklineDays as $day) {
                $temp = $this->myDb->getOne("SELECT SUM(OXTOTALNETSUM) FROM oxorder WHERE (OXORDERDATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($temp == '') {
                    $temp = '0';
                }
                if ($this->dailyNetSalesSparklines == '') {
                    $this->dailyNetSalesSparklines .= (int) $temp;
                } else {
                    $this->dailyNetSalesSparklines .= ',' . (int) $temp;
                }
            }
            $this->_aViewData['dailyNetSalesSparklines'] = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyNetSalesSparklines . '&line=' . str_replace('#','',$this->myConfig->getConfigParam('chooseColor'));
        }

        $this->dailyBrutSales = $this->myDb->getOne("SELECT SUM(OXTOTALBRUTSUM) FROM oxorder WHERE (OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyBrutSales == '') {
            $this->dailyBrutSales = '0,00';
        }
        $this->_aViewData['dailyBrutSales'] = number_format($this->dailyBrutSales,2,',','') . ' ' . $this->dailySalesCurrency;

        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach($this->sparklineDays as $day) {
                $temp = $this->myDb->getOne("SELECT SUM(OXTOTALBRUTSUM) FROM oxorder WHERE (OXORDERDATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($temp == '') {
                    $temp = '0';
                }
                if ($this->dailyBrutSalesSparklines == '') {
                    $this->dailyBrutSalesSparklines .= (int) $temp;
                } else {
                    $this->dailyBrutSalesSparklines .= ',' . (int) $temp;
                }
            }
            $this->_aViewData['dailyBrutSalesSparklines'] = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyBrutSalesSparklines . '&line=' . str_replace('#','',$this->myConfig->getConfigParam('chooseColor'));
        }
    }

    /**
     * delivers todays sold article amount
     */
    protected function getDailyArticlesAmount ()
    {
        $results = $this->myDb->getAll("SELECT SUM(oa.oxamount) as ARTICLESAMOUNT FROM oxorder o, oxorderarticles oa WHERE (o.OXORDERDATE like '" . date('Y-m-d') . "%' AND o.OXSHOPID$this->shopIdWhere AND o.OXID=oa.OXORDERID)");
        foreach($results as $result) {
            $this->dailyArticlesAmount =+ $result['ARTICLESAMOUNT'];
        }
        if ($this->dailyArticlesAmount == '') {
            $this->dailyArticlesAmount = '0';
        }
        $this->_aViewData['dailyArticlesAmount'] = $this->dailyArticlesAmount;
    }

    /**
     * delivers list of todays sold articles
     */
protected function getDailyArticlesList ()
    {
        $this->dailyNetSales = $this->myDb->getOne("SELECT SUM(OXTOTALNETSUM) FROM oxorder WHERE (OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");

        $results = $this->myDb->getAll("SELECT oa.* FROM oxorder o, oxorderarticles oa WHERE (o.OXORDERDATE like '" . date('Y-m-d') . "%' AND o.OXSHOPID$this->shopIdWhere AND o.OXID=oa.OXORDERID)");
        foreach($results as $result) {
            $temp['a_' . $result['OXARTID']]['artnum'] = $result['OXARTNUM'];
            $temp['a_' . $result['OXARTID']]['name'] = $result['OXTITLE'];
            $temp['a_' . $result['OXARTID']]['amount'] =+ $result['OXAMOUNT'];
            $temp['a_' . $result['OXARTID']]['netsum'] =+ $result['OXAMOUNT'] * $result['OXNETPRICE'];
            $temp['a_' . $result['OXARTID']]['brutsum'] =+ $result['OXAMOUNT'] * $result['OXBRUTPRICE'];
        }
        if (count($temp) > 0) {
            foreach ($temp as $value) {
                $value['percent'] = round($value['netsum'] / ($this->dailyNetSales / 100),2) . ' %';
                $value['netsum'] = number_format($value['netsum'],2,',','') . ' ' . $this->dailySalesCurrency;
                $value['brutsum'] = number_format($value['brutsum'],2,',','') . ' ' . $this->dailySalesCurrency;
                $this->dailyArticlesList[] = $value;
            }
        }
        if (count($this->dailyArticleList) < 1) {
            $this->dailyArticleList = 0;
        }
        $this->_aViewData['dailyArticlesList'] = $this->dailyArticlesList;
        unset($temp);
    }

    /**
     * delivers todays used payments
     */
    protected function getDailyPayments ()
    {
        $results = $this->myDb->getAll("SELECT count(o.OXPAYMENTTYPE) as PAYMENTS, SUM(o.OXPAYCOST) as PAYMENTSUM, p.OXDESC from oxorder o, oxpayments p WHERE(o.OXORDERDATE like '" . date('Y-m-d') . "%' AND o.oxpaymenttype=p.oxid AND o.OXSHOPID$this->shopIdWhere) GROUP BY o.OXPAYMENTTYPE ORDER BY PAYMENTS DESC");
        if (count($results) > 0) {
            foreach($results as $result) {
                $sum = +$result['PAYMENTSUM'];
                if ($this->myConfig->getConfigParam('showSparklines')) {
                    foreach ($this->sparklineDays as $day) {
                        $tmp = $this->myDb->getOne("SELECT count(o.OXPAYMENTTYPE) as PAYMENTS from oxorder o, oxpayments p WHERE(o.OXORDERDATE like '" . $day . "%' AND o.oxpaymenttype=p.oxid AND o.OXSHOPID$this->shopIdWhere AND p.OXDESC='" . $result['OXDESC'] . "')");
                        if ($tmp == '') {
                            $tmp = '0';
                        }
                        if ($this->dailyPaymentsAmountSparklines == '') {
                            $this->dailyPaymentsAmountSparklines .= (int)$tmp;
                        } else {
                            $this->dailyPaymentsAmountSparklines .= ',' . (int)$tmp;
                        }
                        $tmp = $this->myDb->getOne("SELECT SUM(o.OXPAYCOST) as PAYMENTSUM from oxorder o, oxpayments p WHERE(o.OXORDERDATE like '" . $day . "%' AND o.oxpaymenttype=p.oxid AND o.OXSHOPID$this->shopIdWhere AND p.OXDESC='" . $result['OXDESC'] . "')");
                        if ($tmp == '') {
                            $tmp = '0';
                        }
                        if ($this->dailyPaymentsSumSparklines == '') {
                            $this->dailyPaymentsSumSparklines .= (int)$tmp;
                        } else {
                            $this->dailyPaymentsSumSparklines .= ',' . (int)$tmp;
                        }
                    }
                    $this->dailyPaymentsAmountSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyPaymentsAmountSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
                    $this->dailyPaymentsSumSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyPaymentsSumSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
                }
                $temp[] = array('type' => $result['OXDESC'], 'amount' => $result['PAYMENTS'], 'sum' => $result['PAYMENTSUM'], 'amountSparkline' => $this->dailyPaymentsAmountSparklines, 'sumSparkline' => $this->dailyPaymentsSumSparklines);
                $this->dailyPaymentsAmountSparklines = '';
                $this->dailyPaymentsSumSparklines = '';
            }
        } else {
            $temp[] = array('type' => '-', 'amount' => '-', 'sum' => '-', 'percent' => '-', 'amountSparkline' => '-', 'sumSparkline' => '-');
        }
        foreach ($temp as $value) {
            if ($value['type'] != '-') {
                $value['percent'] = round($value['sum'] / ($sum / 100),2) . ' %';
                $value['sum'] = number_format($value['sum'],2,',','') . ' ' . $this->dailySalesCurrency;
            }
            $this->dailyPayments[] = $value;
        }
        $this->_aViewData['dailyPayments'] = $this->dailyPayments;
        unset($temp);
        unset($sum);
    }

    /**
     * delivers todays used shippings
     */
    protected function getDailyDeliveries ()
    {
        $results = $this->myDb->getAll("SELECT count(o.OXDELTYPE) as DELIVERIES, SUM(o.OXDELCOST) as DELIVERYSUM, d.OXTITLE from oxorder o, oxdeliveryset d WHERE(o.OXORDERDATE like '" . date('Y-m-d') . "%' AND o.oxdeltype=d.oxid AND o.OXSHOPID$this->shopIdWhere) GROUP BY o.OXDELTYPE ORDER BY DELIVERIES DESC");
        if (count($results) > 0) {
            foreach($results as $result) {
                $sum = +$result['DELIVERIES'];
                if ($this->myConfig->getConfigParam('showSparklines')) {
                    foreach ($this->sparklineDays as $day) {
                        $tmp = $this->myDb->getOne("SELECT count(o.OXDELTYPE) as DELIVERIES from oxorder o, oxdeliveryset d WHERE(o.OXORDERDATE like '" . $day . "%' AND o.oxdeltype=d.oxid AND o.OXSHOPID$this->shopIdWhere AND d.OXTITLE='" . $result['OXTITLE'] . "')");
                        if ($tmp == '') {
                            $tmp = '0';
                        }
                        if ($this->dailyDeliveriesAmountSparklines == '') {
                            $this->dailyDeliveriesAmountSparklines .= (int)$tmp;
                        } else {
                            $this->dailyDeliveriesAmountSparklines .= ',' . (int)$tmp;
                        }
                        $tmp = $this->myDb->getOne("SELECT SUM(o.OXDELCOST) as DELIVERYSUM from oxorder o, oxdeliveryset d WHERE(o.OXORDERDATE like '" . $day . "%' AND o.oxdeltype=d.oxid AND o.OXSHOPID$this->shopIdWhere AND d.OXTITLE='" . $result['OXTITLE'] . "')");
                        if ($tmp == '') {
                            $tmp = '0';
                        }
                        if ($this->dailyDeliveriesSumSparklines == '') {
                            $this->dailyDeliveriesSumSparklines .= (int)$tmp;
                        } else {
                            $this->dailyDeliveriesSumSparklines .= ',' . (int)$tmp;
                        }
                    }
                    $this->dailyDeliveriesAmountSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyDeliveriesAmountSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
                    $this->dailyDeliveriesSumSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyDeliveriesSumSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
                }
                $temp[] = array('type' => $result['OXTITLE'], 'amount' => $result['DELIVERIES'], 'sum' => $result['DELIVERYSUM'], 'amountSparkline' => $this->dailyDeliveriesAmountSparklines, 'sumSparkline' => $this->dailyDeliveriesSumSparklines);
                $this->dailyDeliveriesAmountSparklines = '';
                $this->dailyDeliveriesSumSparklines = '';
            }
        } else {
            $temp[] = array('type' => '-', 'amount' => '-', 'sum' => '-', 'percent' => '-', 'amountSparkline' => '-', 'sumSparkline' => '-');
        }
        foreach ($temp as $value) {
            if ($value['type'] != '-') {
                $value['percent'] = round($value['amount'] / ($sum / 100), 2) . ' %';
                $value['sum'] = number_format($value['sum'],2,',','') . ' ' . $this->dailySalesCurrency;
            }
            $this->dailyDeliveries[] = $value;
        }
        $this->_aViewData['dailyDeliveries'] = $this->dailyDeliveries;
        unset($temp);
        unset($sum);
    }

    /**
     * delivers todays new customers
     */
    protected function getDailyUsers ()
    {
        $this->dailyUsersAmount = $this->myDb->getOne("SELECT count(*) as USERS from oxuser WHERE(OXCREATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyUsersAmount < 1) {
            $this->dailyUsersAmount = 0;
        }
        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach ($this->sparklineDays as $day) {
                $tmp = $this->myDb->getOne("SELECT count(*) as USERS from oxuser WHERE(OXCREATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($tmp == '') {
                    $tmp = '0';
                }
                if ($this->dailyUsersAmountSparklines == '') {
                    $this->dailyUsersAmountSparklines .= (int)$tmp;
                } else {
                    $this->dailyUsersAmountSparklines .= ',' . (int)$tmp;
                }
            }
            $this->dailyUsersAmountSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyUsersAmountSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
        }
        $this->dailyUsersNlSubscribed = $this->myDb->getOne("SELECT count(*) as USERS from oxnewssubscribed WHERE(OXSUBSCRIBED like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyUsersNlSubscribed < 1) {
            $this->dailyUsersNlSubscribed = 0;
        }
        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach ($this->sparklineDays as $day) {
                $tmp = $this->myDb->getOne("SELECT count(*) as USERS from oxnewssubscribed WHERE(OXSUBSCRIBED like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($tmp == '') {
                    $tmp = '0';
                }
                if ($this->dailyUsersNlSubscribedSparklines == '') {
                    $this->dailyUsersNlSubscribedSparklines .= (int)$tmp;
                } else {
                    $this->dailyUsersNlSubscribedSparklines .= ',' . (int)$tmp;
                }
            }
            $this->dailyUsersNlSubscribedSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyUsersNlSubscribedSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
        }
        $this->dailyUsersNlUnsubscribed = $this->myDb->getOne("SELECT count(*) as USERS from oxnewssubscribed WHERE(OXUNSUBSCRIBED like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyUsersNlUnsubscribed < 1) {
            $this->dailyUsersNlUnsubscribed = 0;
        }
        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach ($this->sparklineDays as $day) {
                $tmp = $this->myDb->getOne("SELECT count(*) as USERS from oxnewssubscribed WHERE(OXUNSUBSCRIBED like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($tmp == '') {
                    $tmp = '0';
                }
                if ($this->dailyUsersNlUnsubscribedSparklines == '') {
                    $this->dailyUsersNlUnsubscribedSparklines .= (int)$tmp;
                } else {
                    $this->dailyUsersNlUnsubscribedSparklines .= ',' . (int)$tmp;
                }
            }
            $this->dailyUsersNlUnsubscribedSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyUsersNlUnsubscribedSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
        }
        $this->_aViewData['dailyUsers'] = array('amount'=>$this->dailyUsersAmount, 'nlsubscribed'=>$this->dailyUsersNlSubscribed, 'nlunsubscribed'=>$this->dailyUsersNlUnsubscribed, 'amountSparkline'=>$this->dailyUsersAmountSparklines, 'nlSubscribedSparkline'=>$this->dailyUsersNlSubscribedSparklines, 'nlUnsubscribedSparkline'=>$this->dailyUsersNlUnsubscribedSparklines);
    }

    /**
     * delivers todays given discounts
     */
    protected function getDailyDiscounts ()
    {
        $this->dailyDiscounts = $this->myDb->getOne("SELECT count(*) as DISCOUNTS FROM oxorder WHERE (OXDISCOUNT > 0 AND OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyDiscounts == '') {
            $this->dailyDiscounts = '0';
        }
        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach ($this->sparklineDays as $day) {
                $tmp = $this->myDb->getOne("SELECT count(*) as DISCOUNTS FROM oxorder WHERE (OXDISCOUNT > 0 AND OXORDERDATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($tmp == '') {
                    $tmp = '0';
                }
                if ($this->dailyDiscountsSparklines == '') {
                    $this->dailyDiscountsSparklines .= (int)$tmp;
                } else {
                    $this->dailyDiscountsSparklines .= ',' . (int)$tmp;
                }
            }
            $this->dailyDiscountsSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyDiscountsSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
        }
        $this->dailyDiscountsSum = $this->myDb->getOne("SELECT SUM(OXDISCOUNT) as DISCOUNTS FROM oxorder WHERE (OXDISCOUNT > 0 AND OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyDiscountsSum == '') {
            $this->dailyDiscountsSum = '0';
        }
        $sum = $this->dailyDiscountsSum;
        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach ($this->sparklineDays as $day) {
                $tmp = $this->myDb->getOne("SELECT SUM(OXDISCOUNT) as DISCOUNTS FROM oxorder WHERE (OXDISCOUNT > 0 AND OXORDERDATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($tmp == '') {
                    $tmp = '0';
                }
                if ($this->dailyDiscountsSumSparklines == '') {
                    $this->dailyDiscountsSumSparklines .= (int)$tmp;
                } else {
                    $this->dailyDiscountsSumSparklines .= ',' . (int)$tmp;
                }
            }
            $this->dailyDiscountsSumSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyDiscountsSumSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
        }
        $this->dailyVoucherDiscounts = $this->myDb->getOne("SELECT count(*) as DISCOUNTS FROM oxorder WHERE (OXVOUCHERDISCOUNT > 0 AND OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyVoucherDiscounts == '') {
            $this->dailyVoucherDiscounts = '0';
        }
        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach ($this->sparklineDays as $day) {
                $tmp = $this->myDb->getOne("SELECT count(*) as DISCOUNTS FROM oxorder WHERE (OXVOUCHERDISCOUNT > 0 AND OXORDERDATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($tmp == '') {
                    $tmp = '0';
                }
                if ($this->dailyVoucherDiscountsSparklines == '') {
                    $this->dailyVoucherDiscountsSparklines .= (int)$tmp;
                } else {
                    $this->dailyVoucherDiscountsSparklines .= ',' . (int)$tmp;
                }
            }
            $this->dailyVoucherDiscountsSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyVoucherDiscountsSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
        }
        $this->dailyVoucherDiscountsSum = $this->myDb->getOne("SELECT SUM(OXDISCOUNT) as DISCOUNTS FROM oxorder WHERE (OXVOUCHERDISCOUNT > 0 AND OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        if ($this->dailyVoucherDiscountsSum == '') {
            $this->dailyVoucherDiscountsSum = '0';
        }
        $sum += $this->dailyVoucherDiscountsSum;
        if ($this->myConfig->getConfigParam('showSparklines')) {
            foreach ($this->sparklineDays as $day) {
                $tmp = $this->myDb->getOne("SELECT SUM(OXDISCOUNT) as DISCOUNTS FROM oxorder WHERE (OXVOUCHERDISCOUNT > 0 AND OXORDERDATE like '" . $day . "%' AND OXSHOPID$this->shopIdWhere)");
                if ($tmp == '') {
                    $tmp = '0';
                }
                if ($this->dailyVoucherDiscountsSumSparklines == '') {
                    $this->dailyVoucherDiscountsSumSparklines .= (int)$tmp;
                } else {
                    $this->dailyVoucherDiscountsSumSparklines .= ',' . (int)$tmp;
                }
            }
            $this->dailyVoucherDiscountsSumSparklines = '/modules/hpeterseim/alternativedashboard/src/service/sparkline/sparkline.php?size=60x14&data=' . $this->dailyVoucherDiscountsSumSparklines . '&line=' . str_replace('#', '', $this->myConfig->getConfigParam('chooseColor'));
        }
        if ($sum > 0) {
            $this->dailyDiscountsPercent = round($this->dailyDiscountsSum / ($sum / 100),2) . ' %';
            $this->dailyVoucherDiscountsPercent = round($this->dailyVoucherDiscountsSum / ($sum / 100),2) . ' %';
        } else {
            $this->dailyDiscountsPercent = '-';
            $this->dailyVoucherDiscountsPercent = '-';
        }
        $this->dailyDiscountsSum = number_format($this->dailyDiscountsSum,2,',','') . ' ' . $this->dailySalesCurrency;
        $this->dailyVoucherDiscountsSum = number_format($this->dailyVoucherDiscountsSum,2,',','') . ' ' . $this->dailySalesCurrency;

        $this->_aViewData['dailyDiscounts'] = array(array('type'=> 'general','amount' => $this->dailyDiscounts, 'sum' => $this->dailyDiscountsSum, 'percent' => $this->dailyDiscountsPercent, 'amountSparkline' => $this->dailyDiscountsSparklines, 'sumSparkline' => $this->dailyDiscountsSumSparklines), array('type'=> 'voucher','amount' => $this->dailyVoucherDiscounts, 'sum' => $this->dailyVoucherDiscountsSum, 'percent' => $this->dailyVoucherDiscountsPercent, 'amountSparkline' => $this->dailyVoucherDiscountsSparklines, 'sumSparkline' => $this->dailyVoucherDiscountsSumSparklines));
        unset($sum);
    }

    /**
     * delivers manufacturers of todays sold articles
     */
    protected function getDailyManufacturers ()
    {
        $this->dailyNetSales = $this->myDb->getOne("SELECT SUM(OXTOTALNETSUM) FROM oxorder WHERE (OXORDERDATE like '" . date('Y-m-d') . "%' AND OXSHOPID$this->shopIdWhere)");
        $results = $this->myDb->getAll("SELECT oa.OXNETPRICE,oa.OXBRUTPRICE,oa.OXAMOUNT FROM oxorder o, oxorderarticles oa, oxarticles a WHERE (o.OXORDERDATE like '" . date('Y-m-d') . "%' AND o.OXSHOPID$this->shopIdWhere AND o.OXID=oa.OXORDERID and oa.OXARTID=a.OXID AND a.OXMANUFACTURERID='')");
        foreach($results as $result) {
            $temp['unknown']['name'] = '-';
            $temp['unknown']['amount'] =+ $result['OXAMOUNT'];
            $temp['unknown']['netsum'] =+ $result['OXAMOUNT'] * $result['OXNETPRICE'];
            $temp['unknown']['brutsum'] =+ $result['OXAMOUNT'] * $result['OXBRUTPRICE'];
        }

        $results = $this->myDb->getAll("SELECT m.OXID,m.OXTITLE,oa.OXNETPRICE,oa.OXBRUTPRICE,oa.OXAMOUNT FROM oxorder o, oxorderarticles oa, oxarticles a, oxmanufacturers m WHERE (o.OXORDERDATE like '" . date('Y-m-d') . "%' AND o.OXSHOPID$this->shopIdWhere AND o.OXID=oa.OXORDERID and oa.OXARTID=a.OXID AND a.OXMANUFACTURERID=m.OXID)");
        foreach($results as $result) {
            $temp['a_' . $result['OXID']]['name'] = $result['OXTITLE'];
            $temp['a_' . $result['OXID']]['amount'] =+ $result['OXAMOUNT'];
            $temp['a_' . $result['OXID']]['netsum'] =+ $result['OXAMOUNT'] * $result['OXNETPRICE'];
            $temp['a_' . $result['OXID']]['brutsum'] =+ $result['OXAMOUNT'] * $result['OXBRUTPRICE'];
        }
        if (count($temp) > 0) {
            foreach ($temp as $value) {
                $value['percent'] = round($value['netsum'] / ($this->dailyNetSales / 100),2) . ' %';
                $value['netsum'] = number_format($value['netsum'],2,',','') . ' ' . $this->dailySalesCurrency;
                $value['brutsum'] = number_format($value['netsum'],2,',','') . ' ' . $this->dailySalesCurrency;
                $this->dailyManufacturers[] = $value;
            }
        }
        if (count($this->dailyManufacturers) < 1) {
            $this->dailyManufacturers = '-';
        }
        $this->_aViewData['dailyManufacturers'] = $this->dailyManufacturers;
        unset($temp);
    }

    /**
     * delivers amount of articles, categories, manufacturers and customers
     */
    protected function getBaseData ()
    {
        $this->baseDataSales = $this->myDb->getOne("SELECT count(*) as SALES FROM oxorder WHERE (OXSTORNO != '1' AND OXSHOPID$this->shopIdWhere)");
        if ($this->baseDataSales == '') {
            $this->baseDataSales = '0';
        }

        $this->baseDataArticles = $this->myDb->getOne("SELECT count(*) as ARTICLES from oxarticles WHERE(OXSHOPID$this->shopIdWhere)");
        if ($this->baseDataArticles < 1) {
            $this->baseDataArticles = 0;
        }
        $this->baseDataCategories = $this->myDb->getOne("SELECT count(*) as CATEGORIES from oxcategories WHERE(OXSHOPID$this->shopIdWhere)");
        if ($this->baseDataCategories < 1) {
            $this->baseDataCategories = 0;
        }
        $this->baseDataManufacturers = $this->myDb->getOne("SELECT count(*) as MANUFACTURERS from oxmanufacturers WHERE(OXSHOPID$this->shopIdWhere)");
        if ($this->baseDataManufacturers < 1) {
            $this->baseDataManufacturers = 0;
        }
        $this->baseDataUser = $this->myDb->getOne("SELECT count(*) as USERS from oxuser WHERE(OXSHOPID$this->shopIdWhere)");
        if ($this->baseDataUser < 1) {
            $this->baseDataUser = 0;
        }
        $this->_aViewData['baseData'] = array('sales' => $this->baseDataSales, 'articles'=>$this->baseDataArticles, 'categories'=>$this->baseDataCategories, 'manufacturers'=>$this->baseDataManufacturers, 'user'=>$this->baseDataUser);
    }



}
