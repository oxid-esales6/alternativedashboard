<?php
/**
 * Copyright 2017 by Hannes Peterseim
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Questions, job offers or simply wishes for this product are welcome, please use oxid@petit-souris.de or take a look @ https://gitlab.petit-souris.de for other peaces of software.
 */

$sLangName  = 'Englisch';

// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------

$aLang = array(
    'charset'                                   => 'UTF-8',
    'DASHBOARD_WELCOME'                         => 'Welcome to your personal administration area!',
    'DASHBOARD_DAILYSALES'                      => 'todays sales',
    'DASHBOARD_DAILYSALES_AMOUNT'               => 'sales',
    'DASHBOARD_DAILYSALES_NETSUM'               => 'net sales sum',
    'DASHBOARD_DAILYSALES_BRUTSUM'              => 'gros sales sum',
    'DASHBOARD_DAILYARTICLES_AMOUNT'            => 'sold articles amount',
    'DASHBOARD_DAILYARTICLES_LIST'              => 'list of sold articles',
    'DASHBOARD_DAILYARTICLES_LIST_ARTNUM'       => 'article nr.',
    'DASHBOARD_DAILYARTICLES_LIST_NAME'         => 'article name',
    'DASHBOARD_DAILYARTICLES_LIST_AMOUNT'       => 'amount',
    'DASHBOARD_DAILYARTICLES_LIST_NETSUM'       => 'net sales sum',
    'DASHBOARD_DAILYARTICLES_LIST_BRUTSUM'      => 'gros sales sum',
    'DASHBOARD_DAILYARTICLES_LIST_PROPORTION'   => 'proportion of',
    'DASHBOARD_DAILYDISCOUNTS'                  => 'given disounts',
    'DASHBOARD_DAILYDISCOUNTS_TYPE'             => 'type',
    'DASHBOARD_DAILYDISCOUNTS_AMOUNT'           => 'amount',
    'DASHBOARD_DAILYDISCOUNTS_SUM'              => 'sum',
    'DASHBOARD_DAILYDISCOUNTS_PROPORTION'       => 'proportion of',
    'DASHBOARD_DAILYMANUFACTURERS'              => 'manufacturers',
    'DASHBOARD_DAILYMANUFACTURERS_NAME'         => 'name',
    'DASHBOARD_DAILYMANUFACTURERS_AMOUNT'       => 'amount',
    'DASHBOARD_DAILYMANUFACTURERS_NETSUM'       => 'net sales sum',
    'DASHBOARD_DAILYMANUFACTURERS_BRUTSUM'      => 'gros sales sum',
    'DASHBOARD_DAILYMANUFACTURERS_PROPORTION'   => 'proportion of',
    'DASHBOARD_DAILYPAYMENTS'                   => 'todays used payments',
    'DASHBOARD_DAILYPAYMENTS_TYPE'              => 'type',
    'DASHBOARD_DAILYPAYMENTS_AMOUNT'            => 'amount',
    'DASHBOARD_DAILYPAYMENTS_SUM'               => 'sum',
    'DASHBOARD_DAILYPAYMENTS_PROPORTION'        => 'proportion of',
    'DASHBOARD_DAILYDELIVERIES'                 => 'todays used shipping',
    'DASHBOARD_DAILYDELIVERIES_TYPE'            => 'type',
    'DASHBOARD_DAILYDELIVERIES_AMOUNT'          => 'amount',
    'DASHBOARD_DAILYDELIVERIES_SUM'             => 'sum',
    'DASHBOARD_DAILYDELIVERIES_PROPORTION'      => 'proportion of',
    'DASHBOARD_DAILYUSERS'                      => 'new customers',
    'DASHBOARD_DAILYUSERS_AMOUNT'               => 'amount',
    'DASHBOARD_DAILYUSERS_NLSUBSCRIBED'         => 'newsletter subscribed',
    'DASHBOARD_DAILYUSERS_NLUNSUBSCRIBED'       => 'newsletter unsubscribed',
    'DASHBOARD_BASEDATA'                        => 'base data',
    'DASHBOARD_BASEDATA_SALES'                  => 'sales',
    'DASHBOARD_BASEDATA_ARTICLES'               => 'articles',
    'DASHBOARD_BASEDATA_CATEGORIES'             => 'categories',
    'DASHBOARD_BASEDATA_MANUFACTURERS'          => 'manufacturers',
    'DASHBOARD_BASEDATA_USER'                   => 'customers',

    'SHOP_MODULE_GROUP_adbOptions'              => 'dashboard options',
    'SHOP_MODULE_showDailySales'                => 'shows todays sales',
    'HELP_SHOP_MODULE_showDailySales'           => 'Shows todays sales on the dashboard.',
    'SHOP_MODULE_showDailyArticlesAmount'       => 'show amount of sold articles',
    'HELP_SHOP_MODULE_showDailyArticlesAmount'  => 'Shows amount of sold articles on the dashboard.',
    'SHOP_MODULE_showDailyArticlesList'         => 'show the list of sold articles',
    'HELP_SHOP_MODULE_showDailyArticlesList'    => 'Shows the list of sold articles on the dashboard.',
    'SHOP_MODULE_showDailyDiscounts'            => 'show given discounts',
    'HELP_SHOP_MODULE_showDailyDiscounts'       => 'Shows given disounts on the dashboard.',
    'SHOP_MODULE_showDailyManufacturers'        => 'show manufacturers of todays sold articles',
    'HELP_SHOP_MODULE_showDailyManufacturers'   => 'Shows manufacturers of todays sold articles on the dashboard.',
    'SHOP_MODULE_showDailyPayments'             => 'show todays used payments',
    'HELP_SHOP_MODULE_showDailyPayments'        => 'Shows todays used payments on the dashboard.',
    'SHOP_MODULE_showDailyDeliveries'           => 'show todays used shippings',
    'HELP_SHOP_MODULE_showDailyDeliveries'      => 'Shows todays used shippings on the dashboard.',
    'SHOP_MODULE_showDailyUsers'                => 'show todays new customers',
    'HELP_SHOP_MODULE_showDailyUsers'           => 'Shows todays new customers on the dashboard.',
    'SHOP_MODULE_showBaseData'                  => 'shows amount of articles/categories/manufacturers/customers',
    'HELP_SHOP_MODULE_showBaseData'             => 'Shows amount of articles/categories/manufacturers/customers on the dashboard.',
    'SHOP_MODULE_chooseColor'                   => 'color',
    'HELP_SHOP_MODULE_chooseColor'              => 'Presents tables in the chosen color.',
    'SHOP_MODULE_showSparklines'                => 'show sparklines with the data of the chosen epoch',
    'HELP_SHOP_MODULE_showSparklines'           => 'Presents sparklines with the data of the chosen epoch on the dashboard.',
    'SHOP_MODULE_chooseColor_#808080'           => 'grey',
    'SHOP_MODULE_chooseColor_#000000'           => 'black',
    'SHOP_MODULE_chooseColor_#008000'           => 'green',
    'SHOP_MODULE_chooseColor_#ff0000'           => 'red',
    'SHOP_MODULE_chooseColor_#4169E1'           => 'blue',
    'SHOP_MODULE_chooseColor_#FF1493'           => 'pink',
    'SHOP_MODULE_chooseColor_#FF8C00'           => 'orange',
    'SHOP_MODULE_chooseColor_#FFD700'           => 'gold',
    'SHOP_MODULE_chooseColor_#800000'           => 'brown',
    'SHOP_MODULE_chooseColor_#008B8B'           => 'cyan',
    'SHOP_MODULE_dashboardRefresh'              => 'dashboard refresh',
    'HELP_SHOP_MODULE_dashboardRefresh'         => 'Sets dashboards refresh.',
    'SHOP_MODULE_dashboardRefresh_10'           => '10 seconds',
    'SHOP_MODULE_dashboardRefresh_20'           => '20 seconds',
    'SHOP_MODULE_dashboardRefresh_30'           => '30 seconds',
    'SHOP_MODULE_dashboardRefresh_40'           => '40 seconds',
    'SHOP_MODULE_dashboardRefresh_50'           => '50 seconds',
    'SHOP_MODULE_dashboardRefresh_60'           => '60 seconds',
    'SHOP_MODULE_dashboardRefresh_0'            => 'never',
    'SHOP_MODULE_showSparklines_7'              => 'last 7 days',
    'SHOP_MODULE_showSparklines_14'             => 'last 14 days',
    'SHOP_MODULE_showSparklines_21'             => 'last 21 days',
    'SHOP_MODULE_showSparklines_30'             => 'last 30 days',
    'SHOP_MODULE_showSparklines_0'              => 'no sparklines',
);
