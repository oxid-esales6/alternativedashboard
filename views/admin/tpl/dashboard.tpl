<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>[{ oxmultilang ident="MAIN_TITLE" }]</title>
    <link rel="stylesheet" href="[{$oViewConf->getResourceUrl()}]main.css">
    <link rel="stylesheet" href="[{$oViewConf->getResourceUrl()}]colors.css">
    <style>
        .leftcentered {
            float:left;text-align:center;
        }
        .leftboldcentered {
            float:left;font-weight:bold;text-align:center;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=[{$charset}]">
    [{if $dashboardRefresh > 0}]
        <meta http-equiv="Refresh" content="[{$dashboardRefresh}]">
    [{/if}]
</head>
<body>

<script type="text/javascript">
    parent.sShopTitle = "[{$actshop|oxaddslashes}]";
    parent.setTitle();
</script>

<h1>[{ oxmultilang ident="DASHBOARD_WELCOME" }]</h1>
<hr>

[{if $aMessage }]
    <div class="messagebox">
        [{ oxmultilang ident="MAIN_INFO" }]:<br>
        [{foreach from=$aMessage item=sMessage key=class }]
        <p class="[{$class}]">[{ $sMessage }]</p>
        [{/foreach}]
    </div>
<hr>
    [{/if}]

[{ if $dailyNetSales != '' && $dailyBrutSales != '' }]
    <div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
        <div style="table:cell;">
            <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;margin-inside:1px;text-align:center;">[{ oxmultilang ident="DASHBOARD_DAILYSALES" }]</div>
        </div>
        <div style="table:cell;">
            <div style="height:18px;width:33%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYSALES_AMOUNT" }]</div>
            <div style="height:18px;width:33%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYSALES_NETSUM" }]</div>
            <div style="height:18px;width:34%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYSALES_BRUTSUM" }]</div>
        </div>
        <div style="width:100%;max-height:100px;overflow:auto;margin:0;padding:0;">
            <div style="table:cell;">
                <div style="height:18px;width:33%;" class="leftcentered">[{ if $dailySalesSparklines != '' }]<img src="[{$dailySalesSparklines}]">[{/if}][{$dailySales}]</div>
                <div style="height:18px;width:33%;" class="leftcentered">[{ if $dailyNetSalesSparklines != '' }]<img src="[{$dailyNetSalesSparklines}]">[{/if}][{$dailyNetSales}]</div>
                <div style="height:18px;width:34%;" class="leftcentered">[{ if $dailyBrutSalesSparklines != '' }]<img src="[{$dailyBrutSalesSparklines}]">[{/if}][{$dailyBrutSales}]</div>
            </div>
        </div>
    </div>
    <div style="width:100%;height:2px;"></div>
[{/if}]

[{ if $dailyArticlesList != '' }]
    <div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
        <div style="table:cell;">
            <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;text-align:center;">[{ oxmultilang ident="DASHBOARD_DAILYARTICLES_LIST" }]
                [{ if $dailyArticlesAmount != '' }]
                    ([{ oxmultilang ident="DASHBOARD_DAILYARTICLES_AMOUNT" }] : [{$dailyArticlesAmount}])
                [{/if}]
            </div>
        </div>
        <div style="table:cell;">
            <div style="height:18px;width:10%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYARTICLES_LIST_ARTNUM" }]</div>
            <div style="height:18px;width:40%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYARTICLES_LIST_NAME" }]</div>
            <div style="height:18px;width:10%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYARTICLES_LIST_AMOUNT" }]</div>
            <div style="height:18px;width:15%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYARTICLES_LIST_NETSUM" }]</div>
            <div style="height:18px;width:15%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYARTICLES_LIST_BRUTSUM" }]</div>
            <div style="height:18px;width:10%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYARTICLES_LIST_PROPORTION" }]</div>
        </div>
        <div style="width:100%;max-height:100px;overflow:auto;margin:0;padding:0;">
            [{foreach from=$dailyArticlesList item=article}]
                <div style="table:cell;">
                    <div style="height:18px;width:10%;" class="leftcentered">[{ $article.artnum }]</div>
                    <div style="height:18px;width:40%;" class="leftcentered">[{ $article.name }]</div>
                    <div style="height:18px;width:10%;" class="leftcentered">[{ $article.amount }]</div>
                    <div style="height:18px;width:15%;" class="leftcentered">[{ $article.netsum }]</div>
                    <div style="height:18px;width:15%;" class="leftcentered">[{ $article.brutsum }]</div>
                    <div style="height:18px;width:10%;" class="leftcentered">[{ $article.percent }]</div>
                </div>
            [{/foreach}]
        </div>
    </div>
    <div style="width:100%;height:2px;"></div>
[{/if}]

[{ if $dailyDiscounts != '' }]
    <div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
        <div style="table:cell;">
            <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;text-align:center;">[{ oxmultilang ident="DASHBOARD_DAILYDISCOUNTS" }]</div>
        </div>
        <div style="table:cell;">
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDISCOUNTS_TYPE" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDISCOUNTS_AMOUNT" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDISCOUNTS_SUM" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDISCOUNTS_PROPORTION" }]</div>
        </div>
        <div style="width:100%;max-height:100px;overflow:auto;margin:0;padding:0;">
            [{foreach from=$dailyDiscounts item=discount}]
                <div style="table:cell;">
                    <div style="height:18px;width:25%;" class="leftcentered">[{ $discount.type }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ if $discount.amountSparkline != '-' }]<img src="[{$discount.amountSparkline}]">[{/if}][{ $discount.amount }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ if $discount.sumSparkline != '-' }]<img src="[{$discount.sumSparkline}]">[{/if}][{ $discount.sum }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ $discount.percent }]</div>
                </div>
            [{/foreach}]
        </div>
    </div>
    <div style="width:100%;height:2px;"></div>
[{/if}]

[{ if $dailyManufacturers != '' }]
    <div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
        <div style="table:cell;">
            <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;text-align:center;">[{ oxmultilang ident="DASHBOARD_DAILYMANUFACTURERS" }]</div>
        </div>
        <div style="table:cell;">
            <div style="height:18px;width:50%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYMANUFACTURERS_NAME" }]</div>
            <div style="height:18px;width:10%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYMANUFACTURERS_AMOUNT" }]</div>
            <div style="height:18px;width:15%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYMANUFACTURERS_NETSUM" }]</div>
            <div style="height:18px;width:15%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYMANUFACTURERS_BRUTSUM" }]</div>
            <div style="height:18px;width:10%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDISCOUNTS_PROPORTION" }]</div>
        </div>
        <div style="width:100%;max-height:100px;overflow:auto;margin:0;padding:0;">
            [{foreach from=$dailyManufacturers item=manufacturer}]
                <div style="table:cell;">
                    <div style="height:18px;width:50%;" class="leftcentered">[{ $manufacturer.name }]</div>
                    <div style="height:18px;width:10%;" class="leftcentered">[{ $manufacturer.amount }]</div>
                    <div style="height:18px;width:15%;" class="leftcentered">[{ $manufacturer.netsum }]</div>
                    <div style="height:18px;width:15%;" class="leftcentered">[{ $manufacturer.brutsum }]</div>
                    <div style="height:18px;width:10%;" class="leftcentered">[{ $manufacturer.percent }]</div>
                </div>
            [{/foreach}]
        </div>
    </div>
    <div style="width:100%;height:2px;"></div>
[{/if}]

[{ if !empty($dailyPayments) }]
    <div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
        <div style="table:cell;">
            <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;text-align:center;">[{ oxmultilang ident="DASHBOARD_DAILYPAYMENTS" }]</div>
        </div>
        <div style="table:cell;">
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYPAYMENTS_TYPE" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYPAYMENTS_AMOUNT" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYPAYMENTS_SUM" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYPAYMENTS_PROPORTION" }]</div>
        </div>
        <div style="width:100%;max-height:50px;overflow:auto;margin:0;padding:0;">
            [{foreach from=$dailyPayments item=payment}]
                <div style="table:cell;">
                    <div style="height:18px;width:25%;" class="leftcentered">[{ $payment.type }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ if $payment.amountSparkline != '-' }]<img src="[{$payment.amountSparkline}]">[{/if}][{ $payment.amount }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ if $payment.sumSparkline != '-' }]<img src="[{$payment.sumSparkline}]">[{/if}][{ $payment.sum }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ $payment.percent }]</div>
                </div>
            [{/foreach}]
        </div>
    </div>
    <div style="width:100%;height:2px;"></div>
[{/if}]

[{ if !empty($dailyDeliveries) }]
    <div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
        <div style="table:cell;">
            <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;text-align:center;">[{ oxmultilang ident="DASHBOARD_DAILYDELIVERIES" }]</div>
        </div>
        <div style="table:cell;">
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDELIVERIES_TYPE" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDELIVERIES_AMOUNT" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDELIVERIES_SUM" }]</div>
            <div style="height:18px;width:25%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYDELIVERIES_PROPORTION" }]</div>
        </div>
        <div style="width:100%;max-height:50px;overflow:auto;margin:0;padding:0;">
            [{foreach from=$dailyDeliveries item=delivery}]
                <div style="table:cell;">
                    <div style="height:18px;width:25%;" class="leftcentered">[{ $delivery.type }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ if $delivery.amountSparkline != '-' }]<img src="[{$delivery.amountSparkline}]">[{/if}][{ $delivery.amount }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ if $delivery.sumSparkline != '-' }]<img src="[{$delivery.sumSparkline}]">[{/if}][{ $delivery.sum }]</div>
                    <div style="height:18px;width:25%;" class="leftcentered">[{ $delivery.percent }]</div>
                </div>
            [{/foreach}]
        </div>
    </div>
    <div style="width:100%;height:2px;"></div>
[{/if}]

[{ if $dailyUsers != '' }]
    <div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
        <div style="table:cell;">
            <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;text-align:center;">[{ oxmultilang ident="DASHBOARD_DAILYUSERS" }]</div>
        </div>
        <div style="table:cell;">
            <div style="height:18px;width:33%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYUSERS_AMOUNT" }]</div>
            <div style="height:18px;width:33%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYUSERS_NLSUBSCRIBED" }]</div>
            <div style="height:18px;width:34%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_DAILYUSERS_NLUNSUBSCRIBED" }]</div>
        </div>
        <div style="width:100%;max-height:50px;overflow:auto;margin:0;padding:0;">
            <div style="table:cell;">
                <div style="height:18px;width:33%;" class="leftcentered">[{ if $dailyUsers.amountSparkline != '-' }]<img src="[{$dailyUsers.amountSparkline}]">[{/if}][{ $dailyUsers.amount }]</div>
                <div style="height:18px;width:33%;" class="leftcentered">[{ if $dailyUsers.nlSubscribedSparkline != '-' }]<img src="[{$dailyUsers.nlSubscribedSparkline}]">[{/if}][{ $dailyUsers.nlsubscribed }]</div>
                <div style="height:18px;width:34%;" class="leftcentered">[{ if $dailyUsers.nlUnsubscribedSparkline != '-' }]<img src="[{$dailyUsers.nlUnsubscribedSparkline}]">[{/if}][{ $dailyUsers.nlunsubscribed }]</div>
            </div>
        </div>
    </div>
    <div style="width:100%;height:2px;"></div>
[{/if}]

[{ if $baseData != '' }]
<div style="width:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:1px solid [{$dashboardColor}];">
    <div style="table:cell;">
        <div style="width:100%;float:left;font-weight:bold;background-color:[{$dashboardColor}];color:white;text-align:center;">[{ oxmultilang ident="DASHBOARD_BASEDATA" }]</div>
    </div>
    <div style="table:cell;">
        <div style="height:18px;width:20%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_BASEDATA_SALES" }]</div>
        <div style="height:18px;width:20%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_BASEDATA_ARTICLES" }]</div>
        <div style="height:18px;width:20%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_BASEDATA_CATEGORIES" }]</div>
        <div style="height:18px;width:20%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_BASEDATA_MANUFACTURERS" }]</div>
        <div style="height:18px;width:20%;" class="leftboldcentered">[{ oxmultilang ident="DASHBOARD_BASEDATA_USER" }]</div>
    </div>
    <div style="width:100%;max-height:50px;overflow:auto;margin:0;padding:0;">
        <div style="table:cell;">
            <div style="height:18px;width:20%;" class="leftcentered">[{ $baseData.sales }]</div>
            <div style="height:18px;width:20%;" class="leftcentered">[{ $baseData.articles }]</div>
            <div style="height:18px;width:20%;" class="leftcentered">[{ $baseData.categories }]</div>
            <div style="height:18px;width:20%;" class="leftcentered">[{ $baseData.manufacturers }]</div>
            <div style="height:18px;width:20%;" class="leftcentered">[{ $baseData.user }]</div>
        </div>
    </div>
</div>
<div style="width:100%;height:2px;"></div>
[{/if}]


<script type="text/javascript">
    <!--
    function _homeExpAct(mnid,sbid){
        top.navigation.adminnav._navExtExpAct(mnid,sbid);
    }
    //-->
</script>
</body>
</html>